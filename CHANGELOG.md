# CHANGELOG

<!--- next entry here -->

## 0.3.3
2023-02-24

### Fixes

- switch off correction (8c0e69454712cd7b30db0cd6ae44a2d41a731242)

## 0.3.2
2023-02-14

### Fixes

- correct calculation for correction parameter (bdf6fc8ece742e84b515c7f7c7658b2582aa72a2)

## 0.3.1
2022-12-08

### Fixes

- add log info (ede53d83fd2110559097aba6d59bc5f23c991b17)

## 0.3.0
2022-11-23

### Features

- add correction for consumption (d1916c1fd366d06edab47a2f18e9c30370895bc6)

## 0.2.0
2022-09-22

### Features

- set retain = true for mqtt messages (e47e580596ce63524b5d7e29d12f747b3e07968c)

## 0.1.5
2022-09-20

### Fixes

- update ci/cd (41771286ea09a3fc2c3649256a49805d03f89221)

## 0.1.4
2022-09-20

### Fixes

- update ci/cd (49a14310134ba6dc876ec6b52edfb6a786cb61dd)

## 0.1.3
2022-09-20

### Fixes

- update ci/cd (640c45853a60c1a4f740332b3eb117681aa9680d)

## 0.1.2
2022-09-20

### Fixes

- update ci/cd (09d98cffb207663ceff4a585b7df502271d8425d)

## 0.1.1
2022-09-20

### Fixes

- add build for arm (adabc9aa83b7dcc6357246ead210745eb2e92a90)
- update ci/cd (c3dcf24653031f567d698ed3a3ed01b29b373df4)

## 0.1.0
2022-09-19

### Features

- add sensor functionality (without commands and actions) (3e3ff4d3f117f831cb3f3b319a939e138b589352)
- add switch (12565941d43a5b09b905b8d1c05ea42d50a53bf9)
- add reaction to commands (power, boost) (5644f13e8a9229c23afd7a72b8cd18940aca0d3e)
- add possibility to select mode (7c837a378e25ca64207e82276d6a723b747f53ff)
- make log level configurable (92e20a31df90819a0923b65c171c8088c09b3110)
- ci/cd (69f21e1f5cccd8a96355375eed4b656128258252)
- ci/cd (7e8476cf4f653f46369acc2905289aba9019ad85)

### Fixes

- change default interval (23a02bd9133295ecddf1419755ca93a801af43eb)

