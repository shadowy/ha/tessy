package main

import (
	"gitlab.com/shadowy/go/micro-service/v2"
	"gitlab.com/shadowy/ha/tessy/lib/consul"
	"gitlab.com/shadowy/ha/tessy/lib/service"
	"gitlab.com/shadowy/ha/tessy/lib/settings"
	"gitlab.com/shadowy/ha/tessy/lib/settings/version"
)

const APPLICATION = "tessy-home-assistant"

func main() {
	microservice.InitContextService[*settings.TessyHomeAssistant](
		APPLICATION,
		&settings.TessyHomeAssistant{},
		service.InitWorker,
		new(consul.Service),
		version.Version)
}
