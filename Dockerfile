FROM golang:1.19-alpine as builder
ARG TARGETARCH
RUN apk add --update make git
ENV GO111MODULE=on
WORKDIR /app
COPY . .
RUN go mod vendor
RUN CGO_ENABLED=0 GOOS=linux GOARCH=$TARGETARCH go build -mod=vendor -a -o ./dist/tessy ./cmd/tessy/main.go

FROM alpine:latest
ENV CI_COMMIT_TAG=${version}
WORKDIR /app
RUN mkdir data
VOLUME /app/data
COPY --from=builder /app/dist/tessy ./
COPY ./docker/start.sh ./
RUN chmod 777 / /app/tessy /app/start.sh
CMD /app/start.sh



