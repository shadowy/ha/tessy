binaries=tessy
CI_COMMIT_TAG?=dev

all: lint-full

test:
	@echo ">> test"

lint-full: lint card

card:
	@echo ">> card"
	#@goreportcard-cli -v

lint:
	@echo ">> lint"
	#@golangci-lint run

build: $(binaries)

$(binaries):
	@echo ">>build: $@ $(CI_COMMIT_TAG)"
	@mkdir -p ./dist
	@go build -mod=vendor -o ./dist/linux/amd64/$@ ./cmd/$@/main.go
