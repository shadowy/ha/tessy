#!/bin/sh
CONFIG_FILE="${CONFIG_FILE:-"/app/data/settings.yaml"}"
echo "------------------------------------------------------"
echo "CONFIG_FILE=$CONFIG_FILE"
echo "------------------------------------------------------"
/app/tessy --settings-source=$CONFIG_FILE
