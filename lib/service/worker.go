package service

import (
	"context"
	"github.com/rs/zerolog/log"
	appSettings "gitlab.com/shadowy/go/application-settings/v2/settings"
	microservice "gitlab.com/shadowy/go/micro-service/v2"
	"gitlab.com/shadowy/ha/tessy/lib/settings"
)

func InitWorker(ctx context.Context) error {
	cfg := ctx.Value(microservice.CfgContext).(*appSettings.Configuration[*settings.TessyHomeAssistant])
	service := new(Service)
	service.Init()

	go func() {
		err := service.Start(cfg.Config())
		if err != nil {
			log.Logger.Error().Err(err).Msg("Worker not started properly")
		}
		for s := range cfg.Subscribe() {
			service.Stop()
			err = service.Start(s.Config())
			if err != nil {
				log.Logger.Error().Err(err).Msg("Worker not started properly")
			}
		}
	}()

	return nil
}
