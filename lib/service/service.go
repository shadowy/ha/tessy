package service

import (
	mqtt2 "github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/ha/tessy/lib/api"
	"gitlab.com/shadowy/ha/tessy/lib/api/model"
	"gitlab.com/shadowy/ha/tessy/lib/helpers"
	"gitlab.com/shadowy/ha/tessy/lib/homeassistant"
	"gitlab.com/shadowy/ha/tessy/lib/homeassistant/model/mqtt"
	"gitlab.com/shadowy/ha/tessy/lib/settings"
	"gitlab.com/shadowy/ha/tessy/lib/storage"
	"strings"
	"sync"
	"time"
)

func switchVale(res bool) string {
	if res {
		return "on"
	}
	return "off"
}

type Service struct {
	l              zerolog.Logger
	lock           sync.RWMutex
	devices        []settings.Device
	updateInterval int
	api            *api.TessyClient
	ha             *homeassistant.Client
	dataSet        *storage.DataSet[storage.Device]
	limit          float32
}

func (s *Service) Init() {
	s.api = api.Create()
	go s.getState()
	go s.getConsumption()
}

func (s *Service) Start(cfg *settings.TessyHomeAssistant) error {
	s.l = log.Logger.With().Str("class", "Service").Logger()
	s.l.Info().Msg("Start->")
	s.lock.Lock()
	defer s.lock.Unlock()

	l := cfg.LogLevel
	if l == "" {
		l = "info"
	}
	if level, err := zerolog.ParseLevel(l); err == nil {
		s.l.Info().Str("logLevel", l).Msg("set logLevel")
		zerolog.SetGlobalLevel(level)
	} else {
		s.l.Warn().Err(err).Msg("set logLevel. set info level")
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}

	s.devices = cfg.Devices
	var err error
	s.dataSet, err = cfg.GetDataSet()
	if err != nil {
		return err
	}
	s.limit = cfg.ConsumptionLimit
	if cfg.UpdateInterval == nil {
		s.updateInterval = 1
	} else {
		s.updateInterval = *cfg.UpdateInterval
	}
	s.l = log.Logger.With().
		Str("class", "Service").
		Int("devices-count", len(s.devices)).
		Int("update-interval", s.updateInterval).
		Logger()

	s.ha, err = homeassistant.Create(cfg.MQTT.Host, cfg.MQTT.Port, cfg.MQTT.User, cfg.MQTT.Password)
	if err != nil {
		return err
	}

	if err := s.initDevices(); err != nil {
		go s.Stop()
		return err
	}
	if err = s.registerDevices(); err != nil {
		go s.Stop()
		return err
	}

	log.Logger.Info().Msg("<-Start")
	return nil
}

func (s *Service) Stop() {
	s.l.Info().Msg("Service.Stop")

	s.lock.Lock()
	defer s.lock.Unlock()

	s.devices = nil
	if s.ha != nil {
		s.ha.Destroy()
		s.ha = nil
	}
}

func (s *Service) getState() {
	for {
		s.lock.RLock()
		s.l.Debug().Msg("getState")
		for i := range s.devices {
			device := s.devices[i]
			s.l.Debug().
				Str("host", device.Host).
				Str("id", device.ID).
				Str("name", device.Name).
				Msg("getState device")
			data, err := s.api.GetState(&device)
			if err != nil {
				continue
			}
			s.sendState(&device, data)
			s.sendSwitchState(&device, data)
			s.sendSelectState(&device, data)

		}
		s.lock.RUnlock()
		time.Sleep(time.Minute * time.Duration(s.updateInterval))
	}
}

func (s *Service) getConsumption() {
	for {
		s.lock.RLock()
		s.l.Debug().Msg("getConsumption")
		for i := range s.devices {
			device := s.devices[i]
			s.l.Debug().
				Str("host", device.Host).
				Str("id", device.ID).
				Str("name", device.Name).
				Msg("getConsumption device")
			data, err := s.api.GetConsumption(&device)
			if err != nil {
				continue
			}
			s.sendConsumption(&device, data)
		}
		s.lock.RUnlock()
		time.Sleep(time.Minute * time.Duration(s.updateInterval))
	}
}

func (s *Service) initDevices() error {
	for i := range s.devices {
		s.devices[i].Init()
		err := s.subscribe(&s.devices[i])
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Service) registerDevices() error {
	for i := range s.devices {
		d := s.devices[i]
		list := d.GetHADevice().GetDiscoveryMessages()
		for k := range list {
			err := s.ha.Publish(list[k])
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *Service) subscribe(device *settings.Device) error {
	topic := device.GetHADevice().GetTopic("switch", helpers.PStr("common"), helpers.PStr("power_switch"), "command")
	err := s.ha.Subscribe(
		topic,
		func(client mqtt2.Client, message mqtt2.Message) {
			m := string(message.Payload())
			value := false
			if strings.ToLower(m) == "on" {
				value = true
			}
			s.l.Debug().Str("ID", device.ID).Str("data", m).Msg("->power_switch")
			if data, err := s.api.Power(device, value); err == nil {
				s.sendSwitchState(device, data)
			}
			message.Ack()
		},
	)
	if err != nil {
		s.l.Error().Err(err).Str("ID", device.ID).Str("topic", topic).Str("command", "power_switch").Msg("subscribe")
		return err
	}

	topic = device.GetHADevice().GetTopic("switch", helpers.PStr("common"), helpers.PStr("boost_switch"), "command")
	err = s.ha.Subscribe(
		topic,
		func(client mqtt2.Client, message mqtt2.Message) {
			m := string(message.Payload())
			value := false
			if strings.ToLower(m) == "on" {
				value = true
			}
			s.l.Debug().Str("ID", device.ID).Str("data", m).Bool("value", value).Msg("->boost_switch")
			if data, err := s.api.Boost(device, value); err == nil {
				s.sendSwitchState(device, data)
			}
			message.Ack()
		},
	)
	if err != nil {
		s.l.Error().Err(err).Str("ID", device.ID).Str("topic", topic).Str("command", "boost_switch").Msg("subscribe")
		return err
	}

	topic = device.GetHADevice().GetTopic("select", nil, nil, "command")
	err = s.ha.Subscribe(
		topic,
		func(client mqtt2.Client, message mqtt2.Message) {
			m := string(message.Payload())
			mode := settings.GetPosByMode(m)
			s.l.Debug().Str("ID", device.ID).Int("mode", mode).Str("data", m).Msg("->select")
			if mode >= 0 {
				if data, err := s.api.Mode(device, mode+1); err == nil {
					s.sendSelectState(device, data)
				}
			} else {
				s.l.Warn().Str("ID", device.ID).Int("mode", mode).Str("data", m).Msg("->select mode not found")
			}
			message.Ack()
		},
	)
	if err != nil {
		s.l.Error().Err(err).Str("ID", device.ID).Str("topic", topic).Str("command", "select").Msg("subscribe")
		return err
	}

	return nil
}

func (s *Service) sendState(device *settings.Device, data *model.State) {
	topic := device.GetHADevice().GetTopic("sensor", helpers.PStr("common"), helpers.PStr("state"), "state")
	msg := mqtt.SensorData{
		MsgTopic: topic,
		Data: map[string]interface{}{
			"water_temperature": data.Temperature,
			"ref_temperature":   data.RefTemperature,
			"state":             data.State,
		},
	}
	s.l.Debug().
		Str("host", device.Host).
		Str("id", device.ID).
		Str("name", device.Name).
		Str("topic", topic).
		Interface("msg", msg).
		Msg("sendState")
	_ = s.ha.Publish(&msg)
}

func (s *Service) sendSwitchState(device *settings.Device, data *model.State) {
	topic := device.GetHADevice().GetTopic("switch", helpers.PStr("common"), helpers.PStr("power_switch"), "state")
	msg := mqtt.SensorData{
		MsgTopic: topic,
		Data: map[string]interface{}{
			"power_switch": switchVale(data.PowerState),
			"boost_switch": switchVale(data.Boost),
		},
	}
	s.l.Debug().
		Str("host", device.Host).
		Str("id", device.ID).
		Str("name", device.Name).
		Str("topic", topic).
		Interface("msg", msg).
		Msg("sendSwitchState")
	_ = s.ha.Publish(&msg)
}

func (s *Service) sendConsumption(device *settings.Device, data *model.Consumption) {
	state := s.dataSet.Get(device.ID)
	topic := device.GetHADevice().GetTopic("sensor", helpers.PStr("consumption"), helpers.PStr("total_consumption"), "state")
	old := state.Correction
	if data.Total-state.ConsumptionTotal > s.limit {
		state.Correction = data.Total - state.ConsumptionTotal
	}
	s.l.Warn().
		Float32("limit", s.limit).
		Float32("old", old).
		Float32("new", state.Correction).
		Float32("total", data.Total).
		Float32("consumption", state.ConsumptionTotal).
		Msg("------------------------------------------------------------------")
	msg := mqtt.SensorData{
		MsgTopic: topic,
		Data: map[string]interface{}{
			"total_consumption":  data.Total, // - state.Correction,
			"period_consumption": data.FromLastClear,
		},
	}
	state.ConsumptionTotal = data.Total
	state.ConsumptionPeriod = data.FromLastClear
	err := s.dataSet.Set(device.ID, state)
	if err != nil {
		s.l.Warn().
			Str("host", device.Host).
			Str("id", device.ID).
			Str("name", device.Name).
			Str("topic", topic).
			Interface("msg", msg).
			Interface("state", state).
			Msg("sendConsumption dataSet.Set")
	}
	s.l.Debug().
		Str("host", device.Host).
		Str("id", device.ID).
		Str("name", device.Name).
		Str("topic", topic).
		Interface("msg", msg).
		Msg("sendConsumption")
	_ = s.ha.Publish(&msg)
}

func (s *Service) sendSelectState(device *settings.Device, data *model.State) {
	topic := device.GetHADevice().GetTopic("select", nil, nil, "state")
	msg := mqtt.SensorData{
		MsgTopic: topic,
		Data: map[string]interface{}{
			"value": settings.BoilerModes[data.Mode-1],
		},
	}
	s.l.Debug().
		Str("host", device.Host).
		Str("id", device.ID).
		Str("name", device.Name).
		Str("topic", topic).
		Interface("msg", msg).
		Msg("sendSelect")
	_ = s.ha.Publish(&msg)
}
