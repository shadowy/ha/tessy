package consul

import (
	"gitlab.com/shadowy/go/application-settings/v2/settings"
	"gitlab.com/shadowy/go/micro-service/v2"
	settings2 "gitlab.com/shadowy/ha/tessy/lib/settings"
)

type Service struct {
	ID string
}

func (service *Service) GetServiceInfo() microservice.ServiceInfo {
	return microservice.ServiceInfo{}
}

func (service *Service) GetServiceHealthCheck() []microservice.ServiceHealthCheck {
	return make([]microservice.ServiceHealthCheck, 0)
}

func (service *Service) Init(_ settings.Configuration[*settings2.TessyHomeAssistant]) {
}
