package homeassistant

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	mqtt2 "gitlab.com/shadowy/ha/tessy/lib/homeassistant/model/mqtt"
	"time"
)

type Client struct {
	l             zerolog.Logger
	client        mqtt.Client
	subscriptions []string
}

func Create(host string, port int, user, password string) (*Client, error) {
	res := new(Client)
	res.l = log.Logger.With().Str("host", host).Int("port", port).Str("user", user).Logger()
	opt := mqtt.NewClientOptions().
		AddBroker(fmt.Sprintf("tcp://%s:%d", host, port)).
		SetClientID("tessy-homeassistant").
		SetUsername(user).
		SetPassword(password).
		SetKeepAlive(60 * time.Second).
		SetPingTimeout(1 * time.Second).
		SetAutoReconnect(true)
	res.client = mqtt.NewClient(opt)

	t := res.client.Connect()
	if t.Wait() && t.Error() != nil {
		res.l.Error().Err(t.Error()).Msg("Create connect")
		return nil, t.Error()
	}
	return res, nil
}

func (c *Client) Destroy() {
	for i := range c.subscriptions {
		token := c.client.Unsubscribe(c.subscriptions[i])
		token.Wait()
	}
	c.subscriptions = nil
	c.client.Disconnect(200)
}

func (c *Client) Publish(message mqtt2.Message) error {
	c.l.Debug().Str("topic", message.Topic()).Str("json", message.JSON()).Msg("Publish")
	token := c.client.Publish(message.Topic(), 0, true, message.JSON())
	if token.Wait() && token.Error() != nil {
		c.l.Error().Err(token.Error()).Str("topic", message.Topic()).Str("json", message.JSON()).Msg("Publish")
		return token.Error()
	}
	return nil
}

func (c *Client) Subscribe(topic string, callback mqtt.MessageHandler) error {
	c.l.Debug().Str("topic", topic).Msg("Subscribe")
	token := c.client.Subscribe(topic, 0, callback)
	if token.Wait() && token.Error() != nil {
		c.l.Error().Err(token.Error()).Str("topic", topic).Msg("Subscribe")
		return token.Error()
	}
	c.subscriptions = append(c.subscriptions, topic)
	return nil
}
