package model

import (
	"fmt"
	"gitlab.com/shadowy/ha/tessy/lib/homeassistant/model/mqtt"
)

type Switch struct {
	BaseEntity
	Type  string
	Value *struct {
		ON  string
		OFF string
	}
}

func (s *Switch) Class() string {
	return "switch"
}

func (s *Switch) Discover(device *Device) *mqtt.Discovery {
	ValueON := "on"
	ValueOFF := "off"
	if s.Value != nil && s.Value.ON != "" {
		ValueON = s.Value.ON
	}
	if s.Value != nil && s.Value.OFF != "" {
		ValueOFF = s.Value.OFF
	}
	field := s.getField()
	topics := s.GetTopics(device)

	res := mqtt.Discovery{
		MsgTopic: topics["config"],
		ID:       device.ID,
		Name:     field,
		Topics: map[string]string{
			"state":   topics["state"],
			"command": topics["command"],
		},
		Templates: map[string]string{
			"value": fmt.Sprintf("{{value_json.%s}}", field),
		},
		ExtraFields: map[string]interface{}{
			"unique_id": device.ID + "_" + field,
			"state_on":  ValueON,
			"state_off": ValueOFF,
		},
	}

	addToMapIfNotEmpty(res.ExtraFields, "device_class", s.Type)
	addToMapIfNotEmpty(res.ExtraFields, "icon", s.BaseEntity.Icon)
	return &res

}

func (s *Switch) GetTopics(device *Device) map[string]string {
	return s.BaseEntity.GetTopics(device, s.Class())
}
