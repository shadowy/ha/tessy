package model

func addToMapIfNotNil[T any](data map[string]interface{}, key string, value *T) {
	if value != nil {
		data[key] = *value
	}
}

func addToMapIfNotEmpty(data map[string]interface{}, key string, value string) {
	if value != "" {
		data[key] = value
	}
}

func isnull(value *string, def string) string {
	if value != nil {
		return *value
	}
	return def
}
