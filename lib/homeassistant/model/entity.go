package model

import (
	"fmt"
	"gitlab.com/shadowy/ha/tessy/lib/homeassistant/model/mqtt"
)

type BaseEntity struct {
	Group *string
	Field *string
	Icon  string
}

type Entity interface {
	Base() *BaseEntity
	Class() string
	Discover(device *Device) *mqtt.Discovery
	GetTopics(device *Device) map[string]string
}

type EntityTopicsDefinition struct {
	Specific []string
	Common   []string
}

var entityTypeTopics = map[string]EntityTopicsDefinition{
	"sensor": {
		Specific: []string{"config"},
		Common:   []string{"state"},
	},
	"switch": {
		Specific: []string{"config", "command"},
		Common:   []string{"state"},
	},
	"select": {
		Specific: []string{"config", "command"},
		Common:   []string{"state"},
	},
}

func (b *BaseEntity) Base() *BaseEntity {
	return b
}

func (b *BaseEntity) GetTopics(device *Device, class string) map[string]string {
	p := entityTypeTopics[class]
	res := map[string]string{}
	for i := range p.Common {
		path := p.Common[i]
		res["state"] = fmt.Sprintf("/%s/%s/%s/%s", mqtt.HomeAssistantPrefix, class, b.getTopic(device), path)
	}
	for i := range p.Specific {
		path := p.Specific[i]
		res[path] = fmt.Sprintf("%s/%s/%s_%s/%s", mqtt.HomeAssistantPrefix, class, device.ID, b.getField(), path)
	}
	return res
}

func (b *BaseEntity) getField() string {
	field := "value"
	if b.Field != nil {
		field = *b.Field
	}
	return field
}

func (b *BaseEntity) getTopic(device *Device) string {
	topic := device.ID
	if b.Group != nil {
		topic = topic + "_" + *b.Group
	} else if b.Field != nil {
		topic = topic + "_" + *b.Field
	}
	return topic
}
