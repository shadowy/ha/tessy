package mqtt

type Message interface {
	Topic() string
	JSON() string
}
