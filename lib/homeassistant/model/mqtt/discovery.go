package mqtt

import "encoding/json"

type Discovery struct {
	MsgTopic    string
	Name        string
	ID          string
	BasePath    string
	Topics      map[string]string
	Templates   map[string]string
	ExtraFields map[string]interface{}
}

func fillMap[T any](data map[string]T, obj map[string]interface{}, suffix string) {
	for k := range data {
		obj[k+suffix] = data[k]
	}
}

func (d *Discovery) JSON() string {
	j := map[string]interface{}{}
	j["~"] = d.BasePath
	j["name"] = d.Name
	j["object_id"] = d.ID
	fillMap[string](d.Topics, j, "_topic")
	fillMap[string](d.Templates, j, "_template")
	fillMap[interface{}](d.ExtraFields, j, "")
	res, _ := json.Marshal(&j)
	return string(res)
}

func (d *Discovery) Topic() string {
	return d.MsgTopic
}
