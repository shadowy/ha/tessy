package mqtt

import "encoding/json"

type SensorData struct {
	MsgTopic string
	Data     map[string]interface{}
}

func (d *SensorData) JSON() string {
	j := map[string]interface{}{}
	fillMap[interface{}](d.Data, j, "")
	res, _ := json.Marshal(&j)
	return string(res)
}
func (d *SensorData) Topic() string {
	return d.MsgTopic
}
