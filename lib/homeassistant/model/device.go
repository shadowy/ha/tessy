package model

import (
	"fmt"
	"gitlab.com/shadowy/ha/tessy/lib/homeassistant/model/mqtt"
)

type Device struct {
	ID   string
	Name string
	Info *DeviceInfo

	Sensors  []*Sensor
	Switches []*Switch
	Select   []*Select

	topics map[string]string
}

func (d *Device) Init() {
	d.buildTopic()
}

func (d *Device) GetDiscoveryMessages() []*mqtt.Discovery {
	var list []*mqtt.Discovery
	info := d.info()
	entities := d.entities()
	for k := range entities {
		list = append(list, entities[k].Discover(d))
	}
	if info != nil {
		for k := range list {
			list[k].ExtraFields["device"] = info
		}
	}
	return list
}

func (d *Device) GetTopic(class string, group, field *string, topic string) string {
	return d.topics[d.key(class, group, field, topic)]
}

func (d *Device) key(class string, group, field *string, topic string) string {
	return fmt.Sprintf("%s|%s|%s|%s", class, isnull(group, "-"), isnull(field, "-"), topic)
}

func (d *Device) entities() []Entity {
	var list []Entity
	for k := range d.Sensors {
		list = append(list, d.Sensors[k])
	}
	for k := range d.Switches {
		list = append(list, d.Switches[k])
	}
	for k := range d.Select {
		list = append(list, d.Select[k])
	}
	return list
}

func (d *Device) info() map[string]interface{} {
	if d.Info == nil {
		return nil
	}
	return d.Info.Info()
}

func (d *Device) buildTopic() {
	res := map[string]string{}
	list := d.entities()
	for i := range list {
		e := list[i]
		t := e.GetTopics(d)
		for k := range t {
			res[d.key(e.Class(), e.Base().Group, e.Base().Field, k)] = t[k]
		}
	}
	d.topics = res
}
