package model

import (
	"fmt"
	"gitlab.com/shadowy/ha/tessy/lib/homeassistant/model/mqtt"
)

type Select struct {
	BaseEntity

	Options []string
}

func (s *Select) Class() string {
	return "select"
}

func (s *Select) Discover(device *Device) *mqtt.Discovery {
	field := s.getField()
	topics := s.GetTopics(device)

	res := mqtt.Discovery{
		MsgTopic: topics["config"],
		ID:       device.ID,
		Name:     field,
		Topics: map[string]string{
			"state":   topics["state"],
			"command": topics["command"],
		},
		Templates: map[string]string{
			"value": fmt.Sprintf("{{value_json.%s}}", field),
		},
		ExtraFields: map[string]interface{}{
			"unique_id": device.ID + "_" + field,
			"options":   s.Options,
		},
	}

	addToMapIfNotEmpty(res.ExtraFields, "icon", s.BaseEntity.Icon)
	return &res

}

func (s *Select) GetTopics(device *Device) map[string]string {
	return s.BaseEntity.GetTopics(device, s.Class())
}
