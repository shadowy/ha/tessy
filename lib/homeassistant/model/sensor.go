package model

import (
	"fmt"
	"gitlab.com/shadowy/ha/tessy/lib/homeassistant/model/mqtt"
)

type Sensor struct {
	BaseEntity

	Type              string
	UnitOfMeasurement *string
	StateClass        *string
}

func (s *Sensor) Class() string {
	return "sensor"
}

func (s *Sensor) Discover(device *Device) *mqtt.Discovery {
	field := s.getField()
	topics := s.GetTopics(device)
	res := mqtt.Discovery{
		MsgTopic: topics["config"],
		ID:       device.ID,
		Name:     field,
		Topics: map[string]string{
			"state": topics["state"],
		},
		Templates: map[string]string{
			"value": fmt.Sprintf("{{value_json.%s}}", field),
		},
		ExtraFields: map[string]interface{}{
			"unique_id": device.ID + "_" + field,
		},
	}
	addToMapIfNotEmpty(res.ExtraFields, "device_class", s.Type)
	addToMapIfNotEmpty(res.ExtraFields, "icon", s.BaseEntity.Icon)
	addToMapIfNotNil[string](res.ExtraFields, "unit_of_measurement", s.UnitOfMeasurement)
	addToMapIfNotNil[string](res.ExtraFields, "state_class", s.StateClass)
	return &res
}

func (s *Sensor) GetTopics(device *Device) map[string]string {
	return s.BaseEntity.GetTopics(device, s.Class())
}
