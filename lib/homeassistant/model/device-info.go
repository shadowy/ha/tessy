package model

type DeviceInfo struct {
	Manufacturer     *string
	Model            *string
	Name             *string
	SuggestedArea    *string
	ConfigurationUrl *string
	Connections      [][]string
	Identifiers      []string
	HWVersion        *string
	SWVersion        *string
	ViaDevice        *string
}

func (d *DeviceInfo) Info() map[string]interface{} {
	res := map[string]interface{}{}
	addToMapIfNotNil[string](res, "manufacturer", d.Manufacturer)
	addToMapIfNotNil[string](res, "model", d.Model)
	addToMapIfNotNil[string](res, "name", d.Name)
	addToMapIfNotNil[string](res, "suggested_area", d.SuggestedArea)
	addToMapIfNotNil[string](res, "configuration_url", d.ConfigurationUrl)
	if len(d.Connections) != 0 {
		res["connections"] = d.Connections
	}
	if len(d.Connections) != 0 {
		res["identifiers"] = d.Identifiers
	}
	addToMapIfNotNil[string](res, "hw_version", d.HWVersion)
	addToMapIfNotNil[string](res, "sw_version", d.SWVersion)
	addToMapIfNotNil[string](res, "via_device", d.ViaDevice)
	if len(res) == 0 {
		return nil
	}
	return res
}
