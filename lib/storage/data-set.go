package storage

import (
	"errors"
	"gopkg.in/yaml.v2"
	"io/fs"
	"os"
	"sync"
)

type DataSet[T any] struct {
	file string
	mode fs.FileMode
	data map[string]*T
	mu   sync.Mutex
}

func (ds *DataSet[T]) Get(id string) *T {
	if res, exist := ds.data[id]; exist {
		return res
	}
	res := new(T)
	ds.data[id] = res
	return res
}

func (ds *DataSet[T]) Set(id string, data *T) error {
	ds.data[id] = data
	return ds.write()
}

func (ds *DataSet[T]) open(file string, mode fs.FileMode) error {
	ds.data = map[string]*T{}
	ds.file = file
	ds.mode = mode
	return ds.read()
}

func (ds *DataSet[T]) read() error {
	ds.mu.Lock()
	defer func() {
		ds.mu.Unlock()
	}()
	if fileData, err := os.ReadFile(ds.file); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}
		return err
	} else {
		err = yaml.Unmarshal(fileData, &ds.data)
		return err
	}
}

func (ds *DataSet[T]) write() error {
	ds.mu.Lock()
	defer func() {
		ds.mu.Unlock()
	}()
	if fileData, err := yaml.Marshal(ds.data); err != nil {
		return err
	} else {
		err = os.WriteFile(ds.file, fileData, ds.mode)
		return err
	}
}

func Open[T any](file string, mode fs.FileMode) (*DataSet[T], error) {
	res := new(DataSet[T])
	if err := res.open(file, mode); err != nil {
		return nil, err
	}
	return res, nil
}
