package storage

type Device struct {
	ConsumptionPeriod float32 `yaml:"consumptionPeriod"`
	ConsumptionTotal  float32 `yaml:"consumptionTotal"`
	Correction        float32 `yaml:"correction"`
}
