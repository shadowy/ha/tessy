package model

import (
	"encoding/json"
	"errors"
)

type Int struct {
	Value int
}

func (t *Int) UnmarshalJSON(data []byte) error {
	if data[0] == 34 {
		err := json.Unmarshal(data[1:len(data)-1], &t.Value)
		if err != nil {
			return errors.New("Int: UnmarshalJSON: " + err.Error())
		}
	} else {
		err := json.Unmarshal(data, &t.Value)
		if err != nil {
			return errors.New("Int: UnmarshalJSON: " + err.Error())
		}
	}
	return nil
}
