package model

import (
	"encoding/json"
	"errors"
)

type Power struct {
	Value bool
}

func (t *Power) UnmarshalJSON(data []byte) error {
	var res string
	err := json.Unmarshal(data, &res)
	if err != nil {
		return errors.New("Bool: UnmarshalJSON: " + err.Error())
	}
	t.Value = res == "on"
	return nil
}
