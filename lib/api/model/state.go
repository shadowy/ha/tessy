package model

type State struct {
	State          string
	Mode           int
	ErrorFlag      int
	Boost          bool
	PowerState     bool
	Temperature    float32
	RefTemperature float32
	Power          int
}

type StateData struct {
	State          string `json:"heater_state"` //READY, HEATING
	Mode           Int    `json:"mode"`
	ErrorFlag      Int    `json:"err_flag"`
	Boost          Bool   `json:"boost"`
	PowerState     Power  `json:"power_sw"` //on off
	Temperature    Float  `json:"gradus"`
	RefTemperature Float  `json:"ref_gradus"`
	Power          Int    `json:"watts"`
}

func (s *StateData) GetState() *State {
	var res State
	res.State = s.State
	res.Mode = s.Mode.Value
	res.ErrorFlag = s.ErrorFlag.Value
	res.Boost = s.Boost.Value
	res.PowerState = s.PowerState.Value
	res.Temperature = s.Temperature.Value
	res.RefTemperature = s.RefTemperature.Value
	res.Power = s.Power.Value
	return &res
}
