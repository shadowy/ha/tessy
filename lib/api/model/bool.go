package model

import (
	"encoding/json"
	"errors"
)

type Bool struct {
	Value bool
}

func (t *Bool) UnmarshalJSON(data []byte) error {
	var res int
	if data[0] == 34 {
		err := json.Unmarshal(data[1:len(data)-1], &res)
		if err != nil {
			return errors.New("Bool: UnmarshalJSON: " + err.Error())
		}
	} else {
		err := json.Unmarshal(data, &res)
		if err != nil {
			return errors.New("Bool: UnmarshalJSON: " + err.Error())
		}
	}
	t.Value = res != 0
	return nil
}
