package model

import (
	"encoding/json"
	"errors"
)

type Float struct {
	Value float32
}

func (t *Float) UnmarshalJSON(data []byte) error {
	if data[0] == 34 {
		err := json.Unmarshal(data[1:len(data)-1], &t.Value)
		if err != nil {
			return errors.New("Float: UnmarshalJSON: " + err.Error())
		}
	} else {
		err := json.Unmarshal(data, &t.Value)
		if err != nil {
			return errors.New("Float: UnmarshalJSON: " + err.Error())
		}
	}
	return nil
}
