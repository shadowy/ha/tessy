package model

type Consumption struct {
	Total         float32
	FromLastClear float32
}

type ConsumptionData struct {
	Total         Float `json:"ltc"`
	FromLastClear Float `json:"kwh"` //start from clear date
}

func (s *ConsumptionData) GetConsumption() *Consumption {
	var res Consumption
	res.Total = s.Total.Value
	res.FromLastClear = s.FromLastClear.Value
	return &res
}
