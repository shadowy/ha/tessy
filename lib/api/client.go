package api

import (
	"encoding/json"
	"errors"
	"github.com/go-resty/resty/v2"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/ha/tessy/lib/api/model"
	"gitlab.com/shadowy/ha/tessy/lib/settings"
	"strconv"
	"time"
)

type TessyClient struct {
	client *resty.Client
	l      zerolog.Logger
}

func Create() *TessyClient {
	res := new(TessyClient)
	res.client = resty.New()
	res.l = log.Logger.With().Str("class", "TessyClient").Logger()
	return res
}

func (c *TessyClient) GetState(device *settings.Device) (*model.State, error) {
	resp, err := c.client.R().
		SetQueryParam("_", strconv.FormatInt(time.Now().Unix(), 10)).
		Get(c.getUrl(device, "/status"))
	if err != nil {
		c.l.Error().Err(err).Msg("GetState create request")
		return nil, err
	}
	var res model.StateData
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		c.l.Error().Err(err).Msg("GetState unmarshal")
		return nil, err
	}
	if c.l.GetLevel() == zerolog.DebugLevel {
		c.l.Debug().Interface("data", res.GetState()).Msg("state")
	}
	return res.GetState(), nil
}

func (c *TessyClient) GetConsumption(device *settings.Device) (*model.Consumption, error) {
	resp, err := c.client.R().
		SetQueryParam("_", strconv.FormatInt(time.Now().Unix(), 10)).
		Get(c.getUrl(device, "/calcRes"))
	if err != nil {
		c.l.Error().Err(err).Msg("GetConsumption create request")
		return nil, err
	}
	var res model.ConsumptionData
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		c.l.Error().Err(err).Msg("GetConsumption unmarshal")
		return nil, err
	}
	if c.l.GetLevel() == zerolog.DebugLevel {
		c.l.Debug().Interface("data", res.GetConsumption()).Msg("state")
	}
	return res.GetConsumption(), nil
}

func (c *TessyClient) Power(device *settings.Device, state bool) (*model.State, error) {
	val := "off"
	if state {
		val = "on"
	}
	resp, err := c.client.R().
		SetQueryParam("_", strconv.FormatInt(time.Now().Unix(), 10)).
		SetQueryParam("val", val).
		Get(c.getUrl(device, "/power"))
	if err != nil {
		c.l.Error().Err(err).Msg("Power create request")
		return nil, err
	}
	if resp.IsError() {
		return nil, errors.New(string(resp.Body()))
	}
	return c.GetState(device)
}

func (c *TessyClient) Boost(device *settings.Device, state bool) (*model.State, error) {
	mode := 0
	if state {
		mode = 1
	}
	resp, err := c.client.R().
		SetQueryParam("_", strconv.FormatInt(time.Now().Unix(), 10)).
		SetQueryParam("mode", strconv.Itoa(mode)).
		Get(c.getUrl(device, "/boostSW"))
	if err != nil {
		c.l.Error().Err(err).Msg("Boost create request")
		return nil, err
	}
	if resp.IsError() {
		return nil, errors.New(string(resp.Body()))
	}
	return c.GetState(device)
}

func (c *TessyClient) Mode(device *settings.Device, mode int) (*model.State, error) {
	resp, err := c.client.R().
		SetQueryParam("_", strconv.FormatInt(time.Now().Unix(), 10)).
		SetQueryParam("mode", strconv.Itoa(mode)).
		Get(c.getUrl(device, "/modeSW"))
	if err != nil {
		c.l.Error().Err(err).Msg("Mode switch create request")
		return nil, err
	}
	if resp.IsError() {
		return nil, errors.New(string(resp.Body()))
	}
	log.Logger.Debug().Msg(string(resp.Body()))
	return c.GetState(device)
}

func (c *TessyClient) getUrl(device *settings.Device, path string) string {
	return "http://" + device.Host + path
}
