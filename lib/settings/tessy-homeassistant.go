package settings

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/go/zerolog-settings"
	"gitlab.com/shadowy/ha/tessy/lib/storage"
)

type TessyHomeAssistant struct {
	Log              *zerologsettings.Settings `yaml:"log"`
	LogLevel         string                    `yaml:"logLevel"`
	MQTT             *MQTT                     `yaml:"mqtt"`
	Devices          []Device                  `yaml:"devices"`
	UpdateInterval   *int                      `yaml:"updateInterval"`
	ConsumptionLimit float32                   `yaml:"consumptionLimit"`
}

func (cfg *TessyHomeAssistant) Init() error {
	log.Logger.Info().Msg("TessyHomeAssistant.Init")
	if cfg.ConsumptionLimit == 0 {
		cfg.ConsumptionLimit = 100
	}
	return nil
}

func (cfg *TessyHomeAssistant) GetDataSet() (*storage.DataSet[storage.Device], error) {
	ds, err := storage.Open[storage.Device]("./data/device.yaml", 0644)
	if err != nil {
		log.Logger.Error().Err(err).Str("action", "prepare-storage").Msg("TessyHomeAssistant.GetDataSet")
		return nil, err
	}
	return ds, nil
}
