package settings

var BoilerModes = []string{
	"Manual",
	"Program 1",
	"Program 2",
	"Program 3",
	"Eco",
	"Eco Comfort",
	"Eco Night",
}

func GetPosByMode(data string) int {
	for i := range BoilerModes {
		if data == BoilerModes[i] {
			return i
		}
	}
	return -1
}
