package settings

import (
	"gitlab.com/shadowy/ha/tessy/lib/helpers"
	"gitlab.com/shadowy/ha/tessy/lib/homeassistant/model"
)

type Device struct {
	ID   string `yaml:"id"`
	Name string `yaml:"name"`
	Host string `yaml:"host"`

	haDevice *model.Device
}

func (d *Device) Init() {
	d.buildHADevice()
	d.haDevice.Init()
}

func (d *Device) GetHADevice() *model.Device {
	return d.haDevice
}

func (d *Device) buildHADevice() {
	d.haDevice = &model.Device{
		ID:   d.ID,
		Name: d.Name,
		Info: &model.DeviceInfo{
			Manufacturer:     helpers.PStr("Tesy"),
			Model:            helpers.PStr("MODECO 2"),
			Name:             helpers.PStr(d.Name),
			ConfigurationUrl: helpers.PStr("http://" + d.Host),
			Connections:      [][]string{{"tcp", d.Host}},
			Identifiers:      []string{d.Host},
			ViaDevice:        helpers.PStr("tesy2mqtt"),
		},
		Sensors: []*model.Sensor{
			{
				BaseEntity: model.BaseEntity{
					Group: helpers.PStr("common"),
					Field: helpers.PStr("water_temperature"),
				},
				Type:              "temperature",
				UnitOfMeasurement: helpers.PStr("°C"),
			},
			{
				BaseEntity: model.BaseEntity{
					Group: helpers.PStr("common"),
					Field: helpers.PStr("ref_temperature"),
				},
				Type:              "temperature",
				UnitOfMeasurement: helpers.PStr("°C"),
			},
			{
				BaseEntity: model.BaseEntity{
					Group: helpers.PStr("common"),
					Field: helpers.PStr("state"),
				},
			},
			{
				BaseEntity: model.BaseEntity{
					Group: helpers.PStr("consumption"),
					Field: helpers.PStr("total_consumption"),
				},
				Type:              "energy",
				UnitOfMeasurement: helpers.PStr("kWh"),
				StateClass:        helpers.PStr("total_increasing"),
			},
			{
				BaseEntity: model.BaseEntity{
					Group: helpers.PStr("consumption"),
					Field: helpers.PStr("period_consumption"),
				},
				Type:              "energy",
				UnitOfMeasurement: helpers.PStr("kWh"),
				StateClass:        helpers.PStr("total_increasing"),
			},
		},
		Switches: []*model.Switch{
			{
				BaseEntity: model.BaseEntity{
					Field: helpers.PStr("power_switch"),
					Group: helpers.PStr("common"),
				},
				Type: "outlet",
			},
			{
				BaseEntity: model.BaseEntity{
					Field: helpers.PStr("boost_switch"),
					Group: helpers.PStr("common"),
					Icon:  "mdi:rocket",
				},
			},
		},
		Select: []*model.Select{
			{
				BaseEntity: model.BaseEntity{
					Icon: "mdi:view-list",
				},
				Options: BoilerModes,
			},
		},
	}
}
